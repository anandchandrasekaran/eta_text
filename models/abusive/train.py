from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import Pipeline
import pandas as pd
import pickle

# Creating training data

df = pd.read_csv("labeled_data.csv")

X_train, Y_train = df[:22000]["tweet"],df[:22000]["class"]
X_test, Y_test = df[22000:]["tweet"],df[22000:]["class"]

# Creating pipeline and training

text_clf_pas_agg = Pipeline([
    ('vect', CountVectorizer(ngram_range=(1, 2))),
    ('tfidf', TfidfTransformer()),
    ('clf', PassiveAggressiveClassifier()),
])

text_clf_pas_agg.fit(X_train, Y_train)

# Saving model

filename = 'passagg.sav'
pickle.dump(text_clf_pas_agg, open(filename, 'wb'))