import pickle
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import Pipeline
import re
import string

def clean_input(input_arr):

	#Cleaning the input, removing punctuation urls etc.
	for text in input_arr:
		text = str(text).lower()
		text = re.sub(r"(?:\@|https?\://)\S+", "", text)
		text = text.translate(str.maketrans('', '', string.punctuation))
	return input_arr

def main(input_arr):

	# Loads model using pickle from "passagg.sav"
	input_arr = clean_input(input_arr)
	loaded_model = pickle.load(open("passagg.sav", 'rb'))
	result = loaded_model.predict(input_arr)
	return result

if __name__ == '__main__':
	print(main(["I hate you"]))