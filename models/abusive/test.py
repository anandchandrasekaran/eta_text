import pickle
import pandas as pd
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report

# Classification Method
# Text: "racial slurs" Classified as 0 meaning it is hate speech
# Text: "swear words, insults" Classified as 1 meaning it is offensive language
# Text: "casual talk etc" Classified as 2 meaning it is neither

df = pd.read_csv("labeled_data.csv")

X_test, Y_test = df[22000:]["tweet"],df[22000:]["class"]

loaded_model = pickle.load(open("passagg.sav", 'rb'))
predicted = loaded_model.predict(X_test)
print(accuracy_score(Y_test, predicted))

# Gives accuracy of: 0.9931682322801024
