import spacy
from spacy import displacy
from collections import Counter
import en_core_web_sm

nlp = en_core_web_sm.load()

# Finds details for text such as: Hello my name is Batu and I am inviting you to a meeting at Google, next Wednesday at 7 pm if you would like to join
# Organization Google
# Time 7 pm 
# Date Next Wednesday
# Who Batu   

def main(input_text):

    doc = nlp(input_text)
    arr = [(X, X.ent_iob_, X.ent_type_) for X in doc]
    time = []
    date = []
    organization = []
    people = []
    for item in arr:
        if item[2] == "TIME":
            time.append(str(item[0]))
        if item[2] == "DATE":
            date.append(str(item[0]))
        if item[2] == "ORG":
            organization.append(str(item[0]))
        if item[2] == "PERSON":
            people.append(str(item[0]))

    return " ".join(time), " ".join(date), " ,".join(organization), " ,".join(people)

if __name__ == '__main__':
    print(main('Hello my name is Batu and I am inviting you to a meeting at Google, next Wednesday at 7 pm if you would like to join'))

