import requests
import os

_BERT_SENTIMENT_ANALYSIS = 'https://msd-cvteam-apse.s3.ap-southeast-1.amazonaws.com/Hackathon/bert_sentiment.pt'
_FILE_PATH = os.path.realpath(os.path.dirname(__file__))

try:
    response = requests.get(_BERT_SENTIMENT_ANALYSIS)
    print(response.status_code)
    if response.status_code == 404:
        raise Exception("The object does not exist")
    elif response.status_code != 200:
        raise Exception("Unsuccessful Download")

    if response.status_code == 200:
        print('Download successful. Proceeding to saving the file locally...')
        print(os.path.join(_FILE_PATH, 'weights/bert_sentiment.pt'))
        with open(os.path.join(_FILE_PATH, 'weights/bert_sentiment.pt'), 'wb') as f:
            f.write(response.content)

except Exception as e:
    print("Model wts download failed: ", e)