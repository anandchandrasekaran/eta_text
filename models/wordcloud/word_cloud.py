from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
import matplotlib.pyplot as plt

def word_cloud(input_text):

	wordcloud = WordCloud(stopwords=STOPWORDS, background_color="white").generate(input_text)

	# Display the generated image:
	plt.imshow(wordcloud, interpolation='bilinear')
	plt.axis("off")
	plt.show()

if __name__ == '__main__':
	word_cloud(input_text)