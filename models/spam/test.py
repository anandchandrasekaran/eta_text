import pickle
import pandas as pd
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report

# Classification Method
# Email: enron methanol meter 988291 this is a follow up to the note i gave you on monday... Classified as 0 meaning it is "ham" (not spam)
# Email: hotoshop windows office cheap main trending abasements darer prudently fortuitous... Classified as 1 meaning it is spam

df = pd.read_csv("emails.csv")

X_test, Y_test = df[4000:]["text"],df[4000:]["label_num"]

loaded_model = pickle.load(open("classifier.sav", 'rb'))
predicted = loaded_model.predict(X_test)
print(accuracy_score(Y_test, predicted))

# Gives accuracy of: 0.9931682322801024
