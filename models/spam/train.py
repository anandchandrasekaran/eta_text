from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import Pipeline
import pickle
import pandas as pd

df = pd.read_csv("emails.csv")

# Creating training data

X_train, Y_train = df[:4000]["text"],df[:4000]["label_num"]

# Creating pipeline and training

text_clf_pas_agg = Pipeline([
    ('vect', CountVectorizer(ngram_range=(1, 2))),
    ('tfidf', TfidfTransformer()),
    ('clf', PassiveAggressiveClassifier()),
])

text_clf_pas_agg.fit(X_train, Y_train)

# Saving model

filename = 'passagg.sav'
pickle.dump(text_clf_pas_agg, open(filename, 'wb'))