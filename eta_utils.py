"""
Border module
"""

import cv2
import numpy as np
import os
import time
import logging
logger = logging.getLogger("Sentiment analysis")
import torch.nn as nn
import torch

HIDDEN_DIM = 256
OUTPUT_DIM = 1
N_LAYERS = 2
BIDIRECTIONAL = True
DROPOUT = 0.25

from transformers import BertTokenizer, BertModel
max_input_length = tokenizer.max_model_input_sizes['bert-base-uncased']
bert = BertModel.from_pretrained('bert-base-uncased')
tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
init_token_idx = tokenizer.cls_token_id
eos_token_idx = tokenizer.sep_token_id


class BERTGRUSentiment(nn.Module):
    def __init__(self,
                 bert,
                 hidden_dim,
                 output_dim,
                 n_layers,
                 bidirectional,
                 dropout):
        
        super().__init__()
        
        self.bert = bert
        
        embedding_dim = bert.config.to_dict()['hidden_size']
        
        self.rnn = nn.GRU(embedding_dim,
                          hidden_dim,
                          num_layers = n_layers,
                          bidirectional = bidirectional,
                          batch_first = True,
                          dropout = 0 if n_layers < 2 else dropout)
        
        self.out = nn.Linear(hidden_dim * 2 if bidirectional else hidden_dim, output_dim)
        
        self.dropout = nn.Dropout(dropout)
        
    def forward(self, text):

        with torch.no_grad():
            embedded = self.bert(text)[0]

        _, hidden = self.rnn(embedded)
        
        if self.rnn.bidirectional:
            hidden = self.dropout(torch.cat((hidden[-2,:,:], hidden[-1,:,:]), dim = 1))
        else:
            hidden = self.dropout(hidden[-1,:,:])
        
        output = self.out(hidden)

        return output

model = BERTGRUSentiment(bert,
                         HIDDEN_DIM,
                         OUTPUT_DIM,
                         N_LAYERS,
                         BIDIRECTIONAL,
                         DROPOUT)
weights = os.path.join(_FILE_PATH, 'weights/', weights)
model.load_state_dict(torch.load(weights))
logger.info("Model Loaded")

def predict_sentiment(input_text):
    model.eval()
    start = time.time()
    tokens = tokenizer.tokenize(input_text["mail_text"])
    tokens = tokens[:max_input_length-2]
    indexed = [init_token_idx] + tokenizer.convert_tokens_to_ids(tokens) + [eos_token_idx]
    tensor = torch.LongTensor(indexed)
    tensor = tensor.unsqueeze(0)
    logger.info("time taken text processing : {}".format(time.time()-start))
    prediction = torch.sigmoid(model(tensor))
    return prediction.item()