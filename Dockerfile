FROM python:3.7-slim

ARG PIP_INDEX_URL
RUN pip install --index-url $PIP_INDEX_URL --trusted-host pi.madstreetden.xyz mlaaslib==0.0.5
RUN apt-get update && apt-get install -y libgomp1 libglib2.0-0 libsm6 libxrender1 libxext6 libgl1-mesa-glx vim gcc


RUN mkdir -p /home/ubuntu/mad/


COPY yolact/requirements.txt /home/ubuntu/mad/requirements.txt
RUN pip install -r /home/ubuntu/mad/requirements.txt


COPY yolact/ /home/ubuntu/mad/yolact/


RUN python /home/ubuntu/mad/yolact/download_wts.py
RUN ln -s /home/ubuntu/mad/yolact/main.py /home/ubuntu/mad/main.py
RUN ln -s /home/ubuntu/mad/yolact/sidecar.py /home/ubuntu/mad/sidecar.py

ARG CRED_PATH
COPY ${CRED_PATH} /home/ubuntu/
ENV GOOGLE_APPLICATION_CREDENTIALS="/home/ubuntu/cvteam-google-cloud-cred.json"
ENV OMP_NUM_THREADS=1
ENV MKL_NUM_THREADS=1
ENV OPENBLAS_NUM_THREADS=1
ENV PYTHONPATH="$PYTHONPATH:/home/ubuntu/mad/eta_text/"

ENTRYPOINT ["python", "/home/ubuntu/mad/sidecar.py"]