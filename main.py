"""
Inference code for border

WARNING - This script is meant to be run from two directories above i.e. outside image_utils_v2. All modules are referenced with that in mind.
"""
from eta_text.eta_utils import predict_sentiment

import json
import cv2
import numpy as np
import base64
import copy
import os

import logging
logger = logging.getLogger("Sentiment analysis")

def initialize_node(node_config, **kwargs):
    """
    Initialize node

    Parameters :
    ----------
        kwargs : dict of config args

    Returns :
    -------
        None
    """
    logger.info('Node initialized')

    pass


def check_and_unpack_data(data):
    """
    Validate type and parse. Format for data 

    data = {
          'parent_1': {
              'status': 'STATUS_SUCCESS'/'STATUS_FAILED'/'SKIPPIED',
              'result': result of parent_1 node
          }
          'parent_2': {
              'status': 'STATUS_SUCCESS'/"FAILED"/"SKIPPIED",
              'result': result of parent_2 node
          }    
      }

    Parameters :
    ----------
        data : a dict of parent node results in JSON serialized format

    Returns :
    -------
        parsed_data : a dict of deserialized and verified reults
    """

    if not isinstance(data, dict):
        raise TypeError ('Data not of type dict in node')

    parsed_data = {}
    logger.info("data: {}".format(data))
    logger.info("Packet ID : {}".format(data["packet_id"]))
    data = data["payload"]
    for parent_node, result in data.items():
        try:
            result = result.get('result', '{}')
            parsed_data[parent_node] = copy.deepcopy(result)
        except JSONDecodeError:
            raise ValueError ('JSON Decode error ')

    return parsed_data


def create_arguments_dict(parsed_data):
    """
    Create arguments dict

    Parameters :
    ----------
        parsed_data : dict of parent nodes results

    Returns :
    -------
        arguments_dict : dict of arguments
    """

    arguments_dict = dict()
    input_required_args = ['mail_text']
    for parent_node, result in parsed_data.items():
        for key, value in result.items():
            if key in input_required_args:
                arguments_dict[key] = value

    process_required_args = ['mail_text']
    for arg in process_required_args:
        if arg not in arguments_dict:
            raise KeyError ('Required argument {} not present in arguments list'.format(arg))

    return arguments_dict


def process(data, node_config, **kwargs):
    """
    Process data from parent nodes. Parent node for Border is Background

    Parameters :
    ----------
        data : dict of (parent, result) pairs
        kwargs : dict of config arguments

    Returns :
    -------
        result_dic : dict of result
    """

    parsed_data = check_and_unpack_data(data)
    args = create_arguments_dict(parsed_data)
    logger.info('Processing data...')
    tic=time.time() 
    sentiment = predict_sentiment(**args)
    sentiment_bool = 1 if sentiment>0.5 else 0
    result_dict = dict(sentiment_analysis = sentiment_bool)
    logfer.info("Time taken for prediction: {}".format(tic-time.time()))
    logger.info('Obtained results - {}'.format(border))

    return result_dict


if __name__ == '__main__':
