import logging
import json
import os
from mlaaslib import App, get_config
from main import initialize_node
from main import process as main_process
from utils.global_constants import BORDER_NODE

_ENV_TYPE = os.environ.get('ENV_TYPE')

# Logging
logging.basicConfig()
log = logging.getLogger(BORDER_NODE)
log.setLevel(logging.DEBUG)

# App initialize
node = App('test')

# get config
config = get_config()

# model init
model_dict = initialize_node(config)
log.info("initialize_node done")
log.info("ENV_TYPE is : {}".format(_ENV_TYPE))


@node.process
def process(payload):
    """
    Process data from parent nodes. Parent node for GetText is None.

    Parameters :
    ----------
        data : dict of (parent, result) pairs
        node_config : dict of node config
        kwargs : other params

    Returns :
    -------
        res_dic : dict of result
    """
    result_dict = main_process(payload, model_dict)
    return result_dict


if __name__ == "__main__":
    node.start()
